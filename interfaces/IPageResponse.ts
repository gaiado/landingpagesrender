import Landing from '~~/interfaces/ILanding'
export default interface IPageResponse {
  succeded: boolean
  message: null
  data: Landing
  errors: null
}
