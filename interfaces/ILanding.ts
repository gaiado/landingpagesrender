export default interface ILanding {
    id: number;
    title: string;
    content?: any;
    affiliate: object;
    language: object;
    isPublish:boolean;
    currency: object;
    redirectUrl: string;
    url: string;
    publishAt: Date;
    expiresAt: Date;
    metaDescription: string;
    image: object;
    status: boolean;
    step: number;
  }
  