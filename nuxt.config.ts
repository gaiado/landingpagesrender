export default defineNuxtConfig({
  css: [
    "~/assets/main.css",
    "primevue/resources/themes/saga-blue/theme.css",
    "primevue/resources/primevue.css",
    "primeicons/primeicons.css",
    "primeflex/primeflex.css",
    "@fortawesome/fontawesome-svg-core/styles.css",
    '~/assets/style/variables.scss'
  ],

  build: {
    transpile: [
      "@nuxtjs/style-resources",
      "primevue",
      "@fortawesome/vue-fontawesome",
      "@fortawesome/fontawesome-svg-core",
      "@fortawesome/pro-solid-svg-icons",
      "@fortawesome/pro-regular-svg-icons",
      "@fortawesome/free-brands-svg-icons",
      "ConfirmationService",
    ],
  },
  components: true,
});
