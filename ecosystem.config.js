module.exports = {
    apps: [
      {
        name: 'LandingPagesRender',
        instances: '1', // Or a number of instances
        script: './.output/server/index.mjs',
        args: 'start',
        env: {
            "NUXT_APP_CDN_URL": "http://render.rpoot.com/",
          }
      }
    ]
  }