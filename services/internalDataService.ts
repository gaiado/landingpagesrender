import axios from "axios";
import Landing from "~~/interfaces/ILanding";
import IPageResponse from "~~/interfaces/IPageResponse";

const landingApi = axios.create({
  baseURL:
    process.env.NODE_ENV == "production"
      ? "/api"
      : "http://localhost:5250/api/",
});

const getLanding = async (datos: any): Promise<Landing> => {
  const { data } = await landingApi.get<IPageResponse>(
    "Render?url=" + datos.path + "&hostname=" + datos.host
  );

  if (data.data) {
    data.data.content = JSON.parse(data.data.content);
  }
  return data.data;
};
const getPage = async (id: number): Promise<Landing> => {
  const { data } = await landingApi.get<IPageResponse>("/Pages/" + id);
  if (data.data) {
    data.data.content = JSON.parse(data.data.content);
  }
  return data.data;
};
const getPageSettings = async (id:number): Promise<any> =>{
  const {data} = await landingApi.get<any>("/Render/Settings/"+id)
  if(data.data){
    console.log(data.data)
  }
  return data.data
}

export default {
  getLanding,
  getPage,
  getPageSettings
};
